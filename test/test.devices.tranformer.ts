import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { connect as connectAmqp } from 'amqplib';
import { v4 } from 'uuid';
import { currentId } from 'async_hooks';

const UUID = v4();

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};

const PLETHORA_TOPIC = 'plethora_topic';

const TOPICS = [
    'HOSTNAME',
    'IP_ADDRESS',
    'RAM_HARDWARE',
];
let currentTopic = 0;

function getNextTopic() {
    currentTopic += 1;
    return 'plethora.' + TOPICS[currentTopic % TOPICS.length];
}

function getRandom() {
    return Math.floor(
        Math.random() * 100,
    );
}

connectAmqp(amqpConnectionConf)
    .then(amqpConnection => amqpConnection.createChannel())
    .then((channel) => {
        channel.assertExchange(PLETHORA_TOPIC, 'topic', { durable: false });
        function publishMessage() {
            const currentTopic = getNextTopic();
            channel.publish(
                PLETHORA_TOPIC,
                currentTopic,
                new Buffer(JSON.stringify({
                    device_id: UUID,
                    module_id: currentTopic,
                    timestampe: new Date().getTime(),
                    data: {
                        key: currentTopic.split('.')[1],
                        value: getRandom(),
                    },
                })),
            );
            console.log(
                '[RABBITMQ_LISTENER]#plethora_topic Sent message to %s',
                currentTopic,
            );

            setTimeout(
                publishMessage,
                1000,
            );
        }

        publishMessage();

    })
    .catch(err => console.error(err));
