import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { MongoClient, Db } from 'mongodb';

import { IMessage } from '@plethora/types';
import { AmqpConnection } from '@plethora/utils';

import { Message } from 'amqplib';

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const amqpConnection = new AmqpConnection(amqpConnectionConf);

const exchange = process.env.AMQP_EXCHANGE || 'plethora_topic';

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017M/DB_PLETHORA';
const DEVICES_COLLECTION = 'devices';

const TOPIC_NAMES = (process.env.PLETHORA_MESSAGES || 'HOSTNAME')
    .split(',')
    .map(m => `plethora.${m}`);

MongoClient.connect(mongoUrl)
    .then((db: Db) => Promise.all([db, amqpConnection.retrieveTopicMessage(exchange, TOPIC_NAMES)]))
    .then(([db, msgObservable]) => {
        console.log('listen...');
        msgObservable
            .subscribe((msg: Message) => {
                const parsedMsg: IMessage = JSON.parse(msg.content.toString());
                console.log('Received message', parsedMsg);
                const key = parsedMsg.data.key.toLowerCase();
                db.collection(DEVICES_COLLECTION)
                    .updateOne(
                        { device_id: parsedMsg.device_id },
                        { $set: { [key]: parsedMsg.data.value } },
                        { upsert: true },
                    )
                    .then(() => {
                        console.log(
                            'Updated %s of device %s',
                            key,
                            parsedMsg.device_id,
                        );
                    })
                    .catch((err) => { console.error(err); });
            });
    })
    .catch(err => console.error(err));
